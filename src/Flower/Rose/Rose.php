<?php
/**
 * Created by PhpStorm.
 * User: fitz
 * Date: 2019-04-06
 * Time: 09:00
 */

namespace Flower\Rose;

class Rose
{
    public function desc()
    {
        echo "this", "flower", "is", "Rose";
    }
}