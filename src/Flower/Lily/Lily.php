<?php
/**
 * Created by PhpStorm.
 * User: fitz
 * Date: 2019-04-06
 * Time: 08:58
 */

namespace Flower\Lily;

class Lily
{
    public function desc()
    {
        print "this flower is lily!";
    }

    public function branch()
    {
        echo 'fitz';
    }
}